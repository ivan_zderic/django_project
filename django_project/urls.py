from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from tastypie.api import Api
from accounts.api import UserResource

v1_api = Api(api_name='v1')
v1_api.register(UserResource())

user_resource = UserResource()


urlpatterns = patterns(
    '',
    url(r'^$', 'conversation.views.messages', name='conversation_messages'),
    url(r'^messages/', include('conversation.urls')),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
    url(r'^avatar/', include('avatar.urls')),
)

if settings.DEBUG:
    urlpatterns += patterns(
        'django.views.static', (r'media/(?P<path>.*)',
                                'serve',
                                {'document_root': settings.MEDIA_ROOT}),)
