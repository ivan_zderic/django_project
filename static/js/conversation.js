$(document).ready(function(){
    $('input[placeholder="Search"]').prop('placeholder', 'Search Messages');
    $('.table-hover').removeClass('table-hover');
});

function rowStyle(row, index) {
    if (row['seen'] == false)
        return {classes: 'unread'};
    return {};
}

$(document).on('click', '.message_modal', function(event) {
    var item = $(this).attr('id');
    var row = $(event.target).closest('tr');

    $.ajax({
        type: "GET",
        url: '/messages/view_message/' + item + '/',
        dataType: 'html',
        success: function (data) {
            $('#modal_container').html(data);
            var unread = $(row).hasClass('unread');
            if (unread){
                $(row).removeClass('unread');
                var messages = $('#m_count').text();
                if (messages) {
                    var message_count = parseInt(messages.slice(1,-1));
                    if (message_count > 1)
                        $('#m_count').text('(' + (message_count - 1) + ')');
                    else
                        $('#m_count').text('');
                }
            }
        },
        error: function (data) {
            alert("An error occured");
        },
        complete: function () {
            $('#myModal').modal({show:true});
        }
    });

});