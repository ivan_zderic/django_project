from django.db import models
from django.contrib.auth.decorators import login_required


class TimeStampedModel(models.Model):
    """
    An abstract base class model that provides self-updating 'created' and 'modified' fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class LoginRequiredMixin(object):
    """
    Mixin that provides method that checks if user is logged in.
    """
    @classmethod
    def as_view(cls, **initkwargs):
        """
        Checks if user is logged in.
        """
        view = super(LoginRequiredMixin, cls).as_view(**initkwargs)
        return login_required(view)
