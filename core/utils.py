# -*- coding: utf-8 -*-
"""
Core utilities
"""
from datetime import date, timedelta


def get_first_day_of_month(date_item):
    """
    Returns date of the first day of the month
    """
    return date(date_item.year, date_item.month, 1)


def get_last_day_of_month(date_item):
    """
    Returns date of the last day of the month
    """
    month = date_item.month
    current = date_item

    while month == date_item.month:
        current = current + timedelta(days=1)
        month = current.month

    return current - timedelta(days=1)


def replace_croatian_letters(string):
    """
    Replaces Croatian letters with the corresponding English alphabet characters.
    """
    string = string.encode('utf-8')
    for character, replacement in [
        ['Č', 'C'],
        ['č', 'c'],
        ['Ć', 'C'],
        ['ć', 'c'],
        ['Š', 'S'],
        ['š', 's'],
        ['Đ', 'D'],
        ['đ', 'd'],
        ['Ž', 'Z'],
        ['ž', 'z']
    ]:
        string = string.replace(character, replacement)
    return string
