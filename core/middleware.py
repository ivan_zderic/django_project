from conversation.models import Message


class MessageMiddleware(object):
    """
    Message middleware
    """
    def process_response(self, request, response):
        """
        Update the number of unread messages
        """
        try:
            messages = Message.objects.filter(receiver_id=request.user.id, seen=False).count()
        except:
            messages = 0
        replace_string = '(%d)' % messages if messages else ''
        try:
            response.content = response.content.replace('unread_message_count', replace_string)
        except:
            pass
        return response
