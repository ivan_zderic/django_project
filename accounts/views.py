"""
Accounts app views
"""
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.core.urlresolvers import reverse, reverse_lazy
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView

from .forms import MyRegistrationForm, ChangePasswordForm
from core.models import LoginRequiredMixin


def login(request):
    """
    Login view
    """
    if request.method == 'GET':
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse('conversation_messages'))
        context = {}
        context.update(csrf(request))
        return render(request, 'accounts/login.html', context)
    elif request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            return HttpResponseRedirect(reverse('conversation_messages'))
        else:
            return render(request, 'accounts/login.html', {
                'error': "Invalid login details: {0}, {1}".format(username, password)})


def logout(request):
    """
    Logout view
    """
    auth.logout(request)
    return HttpResponseRedirect(reverse("accounts_login"))


def register_user(request):
    """
    Register user view
    """
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('accounts_register_success'))
        else:
            return render(request, 'accounts/register.html', {'form': form})

    args = {}
    args.update(csrf(request))
    args['form'] = MyRegistrationForm()

    return render(request, 'accounts/register.html', args)


def register_success(request):
    """
    Register success view
    """
    return render(request, 'accounts/register_success.html')


def delete_profile_confirm(request):
    """
    Delete profile confirm view
    """
    return render(request, 'accounts/delete_profile_confirm.html')


def delete_profile(request):
    """
    Delete profile view
    """
    request.user.delete()
    return HttpResponseRedirect(reverse("accounts_login"))


class ChangePasswordView(LoginRequiredMixin, FormView):
    """
    Change password view
    """
    template_name = 'accounts/change_password.html'
    form_class = ChangePasswordForm
    success_url = reverse_lazy('accounts_login')

    def form_valid(self, form):
        """
        If form is valid, set the new password.
        """
        form.current_user.set_password(form.cleaned_data.get('new_password'))
        form.current_user.save()
        return super(ChangePasswordView, self).form_valid(form)

    def get_form_kwargs(self):
        """
        Update keyword arguments with user id.
        """
        kwargs = super(ChangePasswordView, self).get_form_kwargs()
        kwargs.update({'current_user': self.request.user})
        return kwargs


class ViewProfileView(LoginRequiredMixin, TemplateView):
    """
    View profile view
    """
    template_name = "accounts/view_profile.html"

    def get_context_data(self, **kwargs):
        context = super(ViewProfileView, self).get_context_data(**kwargs)
        context['user_profile'] = auth.models.User.objects.get(id=kwargs.get('id'))
        return context


def users(request):
    """
    Manage users
    """
    return render(request, 'accounts/users.html')
