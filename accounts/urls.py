"""
Defines urls for current app
"""
from django.conf.urls import patterns, url

from .views import ChangePasswordView, ViewProfileView


urlpatterns = patterns(
    '',
    url(r'^$', 'accounts.views.login', name='accounts_login'),
    url(r'^login/$', 'accounts.views.login', name='accounts_login'),
    url(r'^logout/$', 'accounts.views.logout', name='accounts_logout'),
    url(r'^register/$', 'accounts.views.register_user', name='accounts_register'),
    url(r'^register_success/$', 'accounts.views.register_success', name='accounts_register_success'),
    url(r'^delete_profile_confirm/$', 'accounts.views.delete_profile_confirm', name='accounts_delete_profile_confirm'),
    url(r'^delete_profile/$', 'accounts.views.delete_profile', name='accounts_delete_profile'),
    url(r'^change_password/$', ChangePasswordView.as_view(), name='accounts_change_password'),
    url(r'^view_profile/(?P<id>\d+)/$', ViewProfileView.as_view(), name='accounts_view_profile'),
    url(r'^users/$', 'accounts.views.users', name='accounts_users'),
)
