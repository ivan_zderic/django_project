from django.contrib.auth.models import User

from tastypie.resources import ModelResource
from tastypie.authorization import Authorization


class UserResource(ModelResource):
    """
    Tastypie resource for User model
    """
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        fields = ['id', 'username', 'first_name', 'last_name']
        authorization = Authorization()
