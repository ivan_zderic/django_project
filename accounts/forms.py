"""
Defines form classes for accounts app.
"""
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.hashers import check_password


class MyRegistrationForm(UserCreationForm):
    """
    Custom user registration form
    """
    email = forms.EmailField(required=True)
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)

    def __init__(self, *args, **kwargs):
        super(MyRegistrationForm, self).__init__(*args, **kwargs)

        # Remove help text
        for fieldname in ['username', 'password1', 'password2']:
            self.fields[fieldname].help_text = None

    class Meta:
        """
        Meta class for the registration form
        """
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()

        return user


class ChangePasswordForm(forms.Form):
    """
    The form for changing password
    """
    current_password = forms.CharField(required=True)
    new_password = forms.CharField(required=True)
    confirm_new_password = forms.CharField(required=True)

    class Meta:
        """
        Meta class for the change password form
        """
        fields = ('current_password', 'new_password', 'confirm_new_password',)

    def __init__(self, *args, **kwargs):
        self.current_user = kwargs.pop('current_user')
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.fields['current_password'].widget.attrs.update({'class': 'form-control'})
        self.fields['new_password'].widget.attrs.update({'class': 'form-control'})
        self.fields['confirm_new_password'].widget.attrs.update({'class': 'form-control'})

        self.fields['current_password'].widget.input_type = 'password'
        self.fields['new_password'].widget.input_type = 'password'
        self.fields['confirm_new_password'].widget.input_type = 'password'

    def clean(self):
        """
        Validate form
        """
        cleaned_data = super(ChangePasswordForm, self).clean()
        current_password = cleaned_data.get('current_password')
        new_password = cleaned_data.get('new_password')
        confirm_new_password = cleaned_data.get('confirm_new_password')

        if current_password and not check_password(current_password, self.current_user.password):
            raise forms.ValidationError("Current password is incorrect.")
        if new_password and confirm_new_password:
            if new_password != confirm_new_password:
                raise forms.ValidationError("New password and confirmed password do not match.")
            else:
                if new_password == current_password:
                    raise forms.ValidationError("New password is the same as current password")

        return cleaned_data
