"""
Conversation app forms
"""
from django.forms import ModelForm
from django.contrib.auth.models import User

from .models import Message


class CreateMessageForm(ModelForm):
    """
    Create new message form
    """

    class Meta:
        model = Message
        fields = ('receiver', 'text')

    def __init__(self, *args, **kwargs):
        current_user_id = kwargs.pop('current_user_id')
        super(CreateMessageForm, self).__init__(*args, **kwargs)

        self.fields['receiver'].widget.attrs['class'] = 'form-control'
        self.fields['text'].widget.attrs['class'] = 'form-control'

        user_data = User.objects.filter(is_staff=False).exclude(id=current_user_id).\
            values('id', 'first_name', 'last_name').order_by('first_name')
        users = [(user['id'], '%s %s' % (user['first_name'], user['last_name'])) for user in user_data]
        users.insert(0, ('', '------'))

        self.fields['receiver'].choices = users


class SendMessageForm(ModelForm):
    """
    Send message to predefined recipient
    """

    class Meta:
        model = Message
        fields = ('text',)

    def __init__(self, *args, **kwargs):
        super(SendMessageForm, self).__init__(*args, **kwargs)
        self.fields['text'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Write new message...', 'rows': 4})
