from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.models import User
from django.db.models import Q
from django.conf import settings
from django.core.paginator import Paginator

from avatar.templatetags.avatar_tags import avatar

from core.models import LoginRequiredMixin
from .forms import CreateMessageForm, SendMessageForm
from .models import Message

import pytz
import json


@login_required
def messages(request):
    """
    Renders list of users who exchanged messages with the current user.
    """
    users = Message.conversations_list(request.user.id)
    return render(request, 'conversation/messages.html', {'users': users})


@login_required
def conversation(request, **kwargs):
    """
    Renders conversation page with specified user.
    """
    user = User.objects.get(id=kwargs['id'])
    form = SendMessageForm()
    return render(request, 'conversation/conversation.html', {'conversation_user': user, 'form': form})


class ConversationView(LoginRequiredMixin, CreateView):
    """
    View class that provides form for sending message and displaying conversation with specified user.
    """
    template_name = "conversation/conversation.html"
    form_class = SendMessageForm

    def form_valid(self, form):
        """
        Called when form is valid. Saves sender and receiver of the message.
        """
        message = form.save(commit=False)
        message.receiver_id = self.kwargs['id']
        message.sender_id = self.request.user.id
        message.save()
        return super(ConversationView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        """
        Adds data to context so that we can access it inside templates.
        """
        context = super(ConversationView, self).get_context_data(**kwargs)
        context['conversation_user'] = User.objects.get(id=self.kwargs['id'])
        return context

    def get_success_url(self):
        """
        Redirect to the same page after success.
        """
        return reverse_lazy('conversation_conversation', kwargs={'id': self.kwargs['id']})


@login_required
def get_messages(request, **kwargs):
    """
    Returns messages exchanged with the other user.
    """
    limit = int(request.GET.get('limit'))
    offset = int(request.GET.get('offset'))

    page_size = limit
    page_number = 1 + offset / page_size

    second_user = kwargs['id']

    fields = [
        'sender__id',
        'sender__first_name',
        'sender__username',
        'id',
        'text',
        'created',
        'seen'
    ]
    condition = (Q(sender_id=request.user.id) & Q(receiver_id=second_user)) | (Q(sender_id=second_user) & Q(receiver_id=request.user.id))
    items = Message.objects.filter(condition).select_related().values(*fields)

    search = request.GET.get('search', None)
    if search:
        items = items.filter(text__icontains=search)

    total = items.count()
    paginator = Paginator(items, page_size)
    objects = paginator.page(page_number)
    default_tz = pytz.timezone(settings.TIME_ZONE)

    rows = [
        {
            'avatar': avatar(message['sender__username'], size=20),
            'sender': 'Me' if message['sender__id'] == request.user.id else message['sender__first_name'],
            'message': '<a id="%s" href="#" class="message_modal">%s</a>' % (message['id'], message['text'] if len(message['text']) < 20 else '%s...' % message['text'][:20]),
            'sent': message['created'].astimezone(default_tz).strftime('%Y-%m-%d %H:%M'),
            'seen': False if not message['seen'] and message['sender__id'] == int(second_user) else True
        } for message in objects
    ]

    return HttpResponse(json.dumps({'rows': rows, 'total': total}))


class CreateMessageView(LoginRequiredMixin, CreateView):
    """
    View class that provides form for creating new message.
    """
    success_url = reverse_lazy('conversation_messages')
    form_class = CreateMessageForm
    template_name = "conversation/create_message.html"

    def form_valid(self, form):
        message = form.save(commit=False)
        message.sender = self.request.user
        message.save()
        return super(CreateMessageView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(CreateMessageView, self).get_form_kwargs()
        kwargs.update({'current_user_id': self.request.user.id})
        return kwargs


@login_required
def view_message(request, **kwargs):
    """
    View message content. Mark the message as read.
    """
    message = Message.objects.get(id=kwargs['id'])
    if not message.seen and message.receiver_id == request.user.id:
        message.seen = True
        message.save()

    return render(request, 'conversation/view_message.html', {'message': message})
