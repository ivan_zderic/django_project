"""
Defines urls for current app
"""
from django.conf.urls import patterns, url

from conversation.views import CreateMessageView
from conversation.views import ConversationView


urlpatterns = patterns(
    '',
    url(r'^$', 'conversation.views.messages', name='conversation_messages'),
    url(r'^create_message/', CreateMessageView.as_view(), name='create_message'),
    url(r'^conversation/(?P<id>\d+)/$', ConversationView.as_view(), name='conversation_conversation'),
    url(r'^get_messages/(?P<id>\d+)/$', 'conversation.views.get_messages', name='conversation_get_messages'),
    url(r'^view_message/(?P<id>\d+)/$', 'conversation.views.view_message', name='conversation_view_message'),
)
