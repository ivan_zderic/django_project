"""
Defines models for current app
"""
from django.db import models
from django.db import connection
from django.contrib.auth.models import User

from core.models import TimeStampedModel

from datetime import datetime
import pytz


class Message(TimeStampedModel):
    """
    Simple Message model
    """
    sender = models.ForeignKey(User, related_name='sender')
    receiver = models.ForeignKey(User, related_name='receiver')
    text = models.TextField(max_length=1024)
    seen = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return self.text

    @staticmethod
    def conversations_list(user_id):
        """
        Returns the list of users who have messages with specified user.
        The list is ordered by the last message date.
        """
        query = 'SELECT subquery.id, subquery.username, subquery.first_name, subquery.last_name, ' \
                'MAX(subquery.date) AS date ' \
                'FROM ' \
                '(SELECT auth_user.id, auth_user.username, auth_user.first_name, auth_user.last_name, ' \
                'MAX(conversation_message.created) AS date ' \
                'FROM auth_user ' \
                'INNER JOIN conversation_message ON auth_user.id = conversation_message.sender_id ' \
                'AND conversation_message.receiver_id = %s ' \
                'GROUP BY auth_user.id, auth_user.first_name, auth_user.last_name ' \
                'UNION ' \
                'SELECT auth_user.id, auth_user.username, auth_user.first_name, auth_user.last_name, ' \
                'MAX(conversation_message.created) AS date ' \
                'FROM auth_user ' \
                'INNER JOIN conversation_message ON auth_user.id = conversation_message.receiver_id ' \
                'AND conversation_message.sender_id = %s ' \
                'GROUP BY auth_user.id, auth_user.first_name, auth_user.last_name ORDER BY date DESC) subquery ' \
                'GROUP BY subquery.id, subquery.first_name, subquery.last_name ' \
                'ORDER BY subquery.date DESC'

        unread_messages = Message.unread_message_count(user_id)

        users = [
            {
                'id': int(user.id),
                'username': user.username,
                'name': '%s %s' % (user.first_name, user.last_name),
                'date': datetime.strptime(user.date, '%Y-%m-%d %H:%M:%S.%f').replace(tzinfo=pytz.utc),
                'unread': unread_messages.get(int(user.id), 0)
            } for user in User.objects.raw(query, [user_id, user_id])
        ]

        return users

    @staticmethod
    def unread_message_count(user_id):
        """
        Returns a dictionary where the key is the id of the other user and the value is number of unread messages
        by the user whose id is the parameter of the function.
        """
        query = 'SELECT conversation_message.sender_id, COUNT(conversation_message.id) FROM conversation_message ' \
                'INNER JOIN auth_user ON auth_user.id = conversation_message.sender_id ' \
                'WHERE conversation_message.receiver_id = %s AND conversation_message.sender_id <> %s ' \
                'AND conversation_message.seen = 0 ' \
                'GROUP BY conversation_message.sender_id'

        cursor = connection.cursor()
        cursor.execute(query, [user_id, user_id])

        return {item[0]: item[1] for item in cursor.fetchall()}
